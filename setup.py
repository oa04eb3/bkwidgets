import os
from setuptools import setup, find_packages

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "bkwidgets",
    version = "0.0.1",
    author = "Olivier Archer",
    author_email = "Olivier.Archer@ifremer.fr",
    description = ("Widgets for scientific data visualisation for standalone bokeh"),
    license = "GPL",
    keywords = "bokeh science",
    url = "https://git.cersat.fr/oarcher/bkwidgets.git",
    packages=find_packages(),
    long_description=read('README.md'),
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Topic :: Scientific/Engineering :: Visualization",
        "Environment :: Web Environment",
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: GNU Lesser General Public License v3 or later (LGPLv3+)",
    ],
    install_requires=[
          'bokeh', 'numpy','future'
    ]
)

