# bkwidgets

provide some high level compounds bokeh widget to be used in bokeh standalone mode (ie html) for scientific data visualisation.

  * scatterplots with selectables axis and colorbar


## Instalation

  ```pip install git+https://gitlab.ifremer.fr/oa04eb3/bkwidgets.git```
  
## Examples

  from 'examples' directory:
  
  ```
    wget https://gitlab.ifremer.fr/oa04eb3/bkwidgets/blob/master/examples/flowers.py
    python flowers.py
  ```
  
