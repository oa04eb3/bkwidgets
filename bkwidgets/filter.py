from __future__ import division
from past.builtins import basestring
from builtins import object
from past.utils import old_div
from bokeh.models import CustomJS, RangeSlider,ColumnDataSource, CustomJSFilter, TextInput
from bokeh.layouts import column
from collections import OrderedDict
import numpy as np
import pdb
import logging


    

logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class Filter(object):
    """
    Class description
    """

    
    def __init__(self, source,cols=None): 
        """Constructor descr"""

        self.widgets=OrderedDict()
        self.filters=[]
        self.widget=None

        self._source=source
        if cols is None:
            cols=list(source.data.keys())
        
        self._cols=cols
        self._range_source=None
        self._string_source=None
        
        
        code_slider= """
            // callback on slider change: just copy new range to range_source
            var column = cb_obj.title;
            var range = cb_obj.value;
            irow=range_source.data['name'].indexOf(column);
            if (irow >= 0) {
                range_source.data['start'][irow]=range[0];
                range_source.data['end'][irow]=range[1];
                range_source.data['activated'][irow]=true;
                //range_source.change.emit(); // really usefull ??
                source.change.emit();
                //for(i=0 ; i < update.lenght ; i++){
                //    update[i].change.emit()
                //}
            } else {
                console.log( irow + ' not found');
            }
        """
    
        code_slider_filter = """
            // return indices in source that match ranges in range_source
        
            var indices = Array(source.get_length()).fill(true);
            
            // get activated filters list
            var activated_filters=[];
            for (var i = 0; i < range_source.get_length(); i++){
                if(range_source.data['activated'][i] ){
                    //activated_filters.push(range_source.data['name'][i])
                    activated_filters.push(i)
                }
            }
            console.log("activated range slider filter : " + activated_filters)
            if (activated_filters.length == 0){
                console.log("no filters => select all")
                return indices;
            }
            
            // iterate through rows of data source
            for (var i = 0; i < indices.length; i++){
                // console.log("checking row: " + i)
                // iterate through activated filters
                selected=true;
                for (var j = 0 ; j <  activated_filters.length; j++){
                    ifilter=activated_filters[j]
                    name=range_source.data['name'][ifilter];
                    start=range_source.data['start'][ifilter];
                    end=range_source.data['end'][ifilter];
                    // console.log("checking col: " + name)
                    if ( (source.data[name][i] < start ) || ( source.data[name][i] > end ) ){
                        // console.log("dropped i=" + i + " " + name + " : " +  source.data[name][i] + " not in [ " + start + " , " + end + " ]")
                        selected=false;
                        break;
                    }
                }
                indices[i]=selected;
            }
            return indices;
        """
        
        code_string= """
            // callback on textinput change: just copy new input to string_source
            var column = cb_obj.title;
            console.log("select string filter change for" + column)
            var input = cb_obj.value;
            irow=string_source.data['name'].indexOf(column);
            if (irow >= 0) {
                string_source.data['input'][irow]=input;
                string_source.data['activated'][irow]=true;
                source.change.emit();
            } else {
                console.log( irow + ' not found');
            }
        """
        
        code_string_filter = """
            // return indices in source that match input in string_source
        
            var indices = Array(source.get_length()).fill(true);
            
            // get activated filters list
            var activated_filters=[];
            for (var i = 0; i < string_source.get_length(); i++){
                if(string_source.data['activated'][i] ){
                    //activated_filters.push(range_source.data['name'][i])
                    activated_filters.push(i)
                }
            }
            console.log("activated string filter : " + activated_filters)
            if (activated_filters.length == 0){
                console.log("no string filters => select all")
                return indices;
            }
            
            // iterate through rows of data source
            for (var i = 0; i < indices.length; i++){
                // console.log("checking row: " + i)
                // iterate through activated filters
                selected=true;
                for (var j = 0 ; j <  activated_filters.length; j++){
                    ifilter=activated_filters[j]
                    name=string_source.data['name'][ifilter];
                    var re = new RegExp(string_source.data['input'][ifilter]);
                    // console.log("checking col: " + name)
                    if (source.data[name][i].search(re) == -1) {
                        console.log("dropped i=" + i + " " + name + " : " +  source.data[name][i] + " doesn't match " + re)
                        selected=false;
                        break;
                    }
                }
                indices[i]=selected;
            }
            return indices;
        """
        
        filter_range_dict={
            "name" : [],
            "activated" : [],
            "start" : [],
            "end"   : [],
            "step"  : []
            }
        filter_string_dict={
            "name" : [],
            "activated" : [],
            "input" : []
            }
        
        # foreach column set up a source with appropriate attributes
        for col in cols:
            # the widget filter depend on column type
            dtype_name=source.data[col].dtype.name
            if ('int' in dtype_name) or ('float' in dtype_name):
                start=np.nanmin(source.data[col])
                end=np.nanmax(source.data[col])
                step=old_div((end-start),100)
                if step==0:
                    step=1
                if start < end:
                    filter_range_dict['name'].append(col)
                    filter_range_dict['activated'].append(False)
                    filter_range_dict['start'].append(start)
                    filter_range_dict['end'].append(end)
                    filter_range_dict['step'].append(step)
                logger.info("range for %s : [ %f , %f ]" % (col , start, end))
            elif 'object' in dtype_name:
                logger.info('got object type for %s' % col)
                elt=source.data[col][0]
                if isinstance(elt, basestring):
                    logger.info("got string type for %s" % col)
                    filter_string_dict['name'].append(col)
                    filter_string_dict['activated'].append(False)
                    filter_string_dict['input'].append(".*")
                else:
                    logger.info("skipping unknown nested object type %s for %s" % (dtype_name,col))
                    #pdb.set_trace()
            else:
                logger.info("skipping unknown type %s for %s" % (dtype_name,col))
                source.remove(col)
                    
                
        
           
        self._range_source=ColumnDataSource(data=filter_range_dict)
        self._string_source=ColumnDataSource(data=filter_string_dict)
        
        callback_filter_slider = CustomJS(args=dict(source=source, range_source=self._range_source), code=code_slider)
        callback_filter_string = CustomJS(args=dict(source=source, string_source=self._string_source), code=code_string)
        
        
        #for irow,name in enumerate(self._range_source.data['name']):
        for col in cols:
            widget=None
            if col in self._range_source.data['name']:
                irow=self._range_source.data['name'].index(col)
                start=self._range_source.data['start'][irow]
                end=self._range_source.data['end'][irow]
                activated=self._range_source.data['activated'][irow]
                step=self._range_source.data['step'][irow]
                widget=RangeSlider(start=start, end=end, value=(start,end), step=step, title=col, callback=callback_filter_slider, callback_policy='mouseup')
            elif col in self._string_source.data['name']:
                irow=self._string_source.data['name'].index(col)
                widget=TextInput(value=self._string_source.data['input'][irow],title=col,callback=callback_filter_string)
            if widget:
                self.widgets[col]=widget
        
        self.widget=column(list(self.widgets.values()))    
        
        
        self.filters.append(CustomJSFilter(args=dict(range_source=self._range_source), code=code_slider_filter))
        self.filters.append(CustomJSFilter(args=dict(string_source=self._string_source), code=code_string_filter))
            
        
        
