from builtins import str
from builtins import object
from bokeh.plotting import Figure
from bokeh.models import CustomJS, LinearAxis, ColorBar, LinearColorMapper, BasicTicker, CDSView
from bokeh.models.widgets import Select, CheckboxGroup, RangeSlider
from bokeh.layouts import column,row,layout
from bokeh.models.transforms import CustomJSTransform
from bokeh.transform import field
from collections import OrderedDict
import numpy as np
import logging

logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class ScatterAxis(object):
    """Setup a bokeh Figure with colorbar and axis selector"""
    def __init__(self, *args, **kwargs):
        """
            kwargs:
                [xycs]_cols : list of column name available to selection for [xycs] axis
                x : default column for x axis ( if None, take the first from x_cols)
                y : idem from y axis
                c : idem for color
                s : idem for size
                size_range : size min/max : default to [1,20]
                scatter_types : list of choosable scatter type ie ['X,Y','X,X-Y']
                    if only one is provided, widget is masked.
                    all available type by default.
                match_aspect : Equal scale on both x & y axis. True , False or None. If None, provide a check box to switch
                source :  bokeh cds
                view : bokeh cdsview (optionnal)
                realize : True or False, default True.
                        If False call to ScatterAxis.realize() is needed to setup widgets.
                        One wan't to delay object realization to change internal attributes.
                remainings kwargs are passed to bokeh.plotting.Figure().scatter()
                
            Attributes defined after __init__ : 
                - self._color_mapper : LinearColorMapper instance
                - self._colorbar : ColorBar instance
                - self._[xyc]axis_select : Select instance
                - self._transform_[xycs] : transform 
                
            Attributes defined after self.realize() :
                - self.widget : the main widget
                
        """
        
        self.fig = None
        self.widget = None
        self.scatter = None
        self.x = None
        self.y = None
        self.c = None
        self.s = None
        self.x_cols = None
        self.y_cols = None
        self.c_cols = None
        self.s_cols = None
        
        self.size_range=[1,20]
        
        self._realize = True
        self._xaxis = None
        self._yaxis=None      
        self._color_mapper=None
        self._colorbar=None
        self._fig_match_aspect = None
        self._xaxis_select = None
        self._yaxis_select = None
        self._caxis_select = None
        self._size_select = None
        self._size_slider = None
        self._fig_match_aspect_chekbox = None
        self._scatter_types= None
        self._scatter_type_select = None
        self._scatter_type_transform_y = None
        self._scatter_type_transform_x = None
        self._transform_s_js = None
        
        self._transform_x = None
        self._transform_y = None
        self._transform_c = None
        self._transform_s = None
        
        self._view = None
        self._source = None
        
        self.__codex = None
        self.__codey = None
        self.__codec = None
        self.__codes = None
        self.__code_fig_match_aspect = None
        self.__kwargs = None
        
        self._source=kwargs.pop("source")
        
        
        if "view" in kwargs:
            self._view=kwargs.pop("view")
            #self._source=self._view.source
        else:
            #self._source=kwargs.pop("source")
            self._view = CDSView(source=self._source, filters=[])
        
        self.size_range=kwargs.pop("size_range",self.size_range)
        
        # get numerics columns from source (TODO : also datetime)     
        cols=sorted(self._source.data.keys())
        #cols.remove('index')
        # numeric and string keys are handled differently
        cols_ok=[]
        for col in cols:
            dtype_name=self._source.data[col].dtype.name
            logger.info("dtype %s for %s" % (dtype_name,col))
            if ("int" in dtype_name) or ("float" in dtype_name): 
                cols_ok.append(col)
    
            #pdb.set_trace()
        
        self.x_cols=kwargs.pop("x_cols",None)
        self.y_cols=kwargs.pop("y_cols",None)
        self.c_cols=kwargs.pop("c_cols",None)
        self.s_cols=kwargs.pop("s_cols",None)
        
        
        if self.x_cols is None:
            self.x_cols=cols_ok
        if self.y_cols is None:
            self.y_cols=cols_ok
        if self.c_cols is None:
            self.c_cols=cols_ok
        if self.s_cols is None:
            self.s_cols=cols_ok
        
        self.x=kwargs.pop("x",None)
        self.y=kwargs.pop("y",None)
        self.c=kwargs.pop("c",None)
        self.s=kwargs.pop("s",None)
        
        if not self.x and self.x_cols:
            self.x=self.x_cols[0]
        if not self.y and self.y_cols:
            self.y=self.y_cols[0]
        if not self.c and self.c_cols:
            self.c=self.c_cols[0]
        if not self.s and self.s_cols:
            self.s=self.s_cols[0]
        
        source_cols=list(self._source.data.keys())
        # args validity check
        if self.x not in source_cols:
            raise IndexError('%s not in source. Allowed: %s' % (self.x, str(source_cols)))
        
        if self.y not in source_cols:
            raise IndexError('%s not in source. Allowed: %s' % (self.y, str(source_cols)))
        
        if self.c and self.c not in source_cols:
            raise IndexError('%s not in source. Allowed: %s' % (self.c, str(source_cols)))
        
        if self.s and self.s not in source_cols:
            raise IndexError('%s not in source. Allowed: %s' % (self.s, str(source_cols)))
        
        # default axis are to be redefined, because bokeh provide no way 
        # to change label from built in figure axis. ( https://stackoverflow.com/questions/27548153/adjusting-x-axis-label-or-y-axis-label-font-font-size-bokeh )
        self._xaxis = LinearAxis(axis_label=self.x)
        self._yaxis = LinearAxis(axis_label=self.y)
        
        self._yaxis_select = Select(title="Y axis:", value=self.y, options=self.y_cols)
        
        self._xaxis_select = Select(title="X axis:", value=self.x, options=self.x_cols)
        
        if self.c:
            self._caxis_select = Select(title="Color:", value=self.c, options=self.c_cols)
        
        if self.s:
            self._size_select = Select(title="Size:", value=self.s, options=self.s_cols)
        
            self._size_slider = RangeSlider(start=self.size_range[0], end=self.size_range[1], value=self.size_range, step=1, title="Size min/max")
        
        self._fig_match_aspect = kwargs.pop('match_aspect',None)
        
        self._fig_match_aspect_chekbox = CheckboxGroup(labels=["Match aspect"])
        if (self._fig_match_aspect is True) or (self._fig_match_aspect is None):
            self._fig_match_aspect_chekbox.active=[0]
            
        self._color_mapper=LinearColorMapper(palette="Viridis256")#, low=np.min(self._view.source.data[self.c]), high=np.max(self._view.source.data[self.c]))
        self._colorbar = ColorBar(color_mapper=self._color_mapper, ticker=BasicTicker(),label_standoff=12, border_line_color=None, location=(0,0))
        
        
        # scatter type selection are based on CustomJSTransform
        self._scatter_types=kwargs.pop('scatter_types',["X,Y","X,X-Y"])
        self._scatter_type_select = Select(title='Scatter type', value=self._scatter_types[0],options=self._scatter_types,callback=CustomJS(args=dict(view=self._view),code="console.log('select') ; view.source.change.emit();"))

        self._scatter_type_transform_y=CustomJSTransform()
        self._scatter_type_transform_y.func="debugger ; return 0; // not implemented"
        self._scatter_type_transform_y.v_func="""
            //debugger;
            console.log("scatter type : " + scatter_type_select.value)
            switch ( scatter_type_select.value ) {
                case "X,Y":
                    return xs;
                    break;
                case "X,X-Y":
                    var t = Array(xs.length);
                    var xcol = xaxis_select.value;
                    for (i=0 ; i < xs.length ; i ++){
                        t[i]=view.source.data[xcol][i]-xs[i];
                    }
                    yaxis.axis_label=xaxis_select.value + " - " + yaxis_select.value
                    return t;
                    break;
                default:
                    console.log('scattter type ' + scatter_type_select.value + ' is not implemented')
            }
        """
        self._scatter_type_transform_y.args=dict(view=self._view,xaxis_select=self._xaxis_select,yaxis_select=self._yaxis_select,yaxis=self._yaxis,xaxis=self._xaxis,caxis=self._caxis_select,scatter_type_select=self._scatter_type_select)
        
        
        self._transform_s_js=CustomJSTransform()
        self._transform_s_js.func="debugger ; return 0; // not implemented"
        self._transform_s_js.v_func="""
            var column = size_select.value;
            console.log("size : " + column)
            // size_slider
            values=view.source.data[column]
            min_value=Math.min.apply(Math, values.filter(Boolean));
            max_value=Math.max.apply(Math, values.filter(Boolean));
            min_size=size_slider.value[0]
            max_size=size_slider.value[1]
            var i = values.length
            var size =  Array(i)
            while(i--){
                size[i]=Math.ceil(min_size + ( (values[i] - min_value)  / (max_value - min_value ) * (max_size - min_size) ))
            }
            return size;
        """
        self._transform_s_js.args=dict(size_select=self._size_select,size_slider=self._size_slider,view=self._view)
        
        self.__codex = """
            var column = cb_obj.value;
            console.log("x axis : " + column)
            scatter.glyph.x.field = column;
            axis.axis_label = column;
            scatter.glyph.change.emit();
        """
        self.__codey = """
            var column = cb_obj.value;
            console.log("y axis : " + column)
            scatter.glyph.y.field = column;
            axis.axis_label = column;
            scatter.glyph.change.emit();
        """
        
        self.__codes = """
            scatter.glyph.change.emit();
        """
        
        self.__codec = """
            var column = cb_obj.value;
            console.log("c axis : " + column)
            //debugger;
            var cvalues=view.source.data[column];
            var selected=view.indices;
            cvalues=selected.map(i => cvalues[i]); // cvalues=cvalues[selected]
            
            scatter.glyph.fill_color.field = column;
            color_mapper.high=Math.max.apply(Math, cvalues.filter(Boolean));
            color_mapper.low=Math.min.apply(Math, cvalues.filter(Boolean));
            console.log("colorbar : " + column + " [ " + color_mapper.low + " , " + color_mapper.high + " ]")
            fig.title.text = column;
            color_mapper.change.emit()
        """
        
        
        
        self.__code_fig_match_aspect = """
            if(cb_obj.active[0] == 0){
                fig.match_aspect=true;
            } else {
                fig.match_aspect=false;
            }
            // redraw
            scatter.glyph.change.emit();
        """
        
        # remaining kwargs
        self.__kwargs = kwargs
        
        if self._realize:
            self.realize()
        
        
    def realize(self):   
        """widget generation. by default, widgets are automatically generated by __init__"""
        
         
        # figure title is used as a colorbar title
        self.fig= Figure(title=self.c,title_location='right',toolbar_location='above')
        self.fig.title.align="center"
        self.fig.title.text_font_style="normal"
        self.fig.x_range.range_padding = 0
        self.fig.y_range.range_padding = 0
        
        # provide our own axis (https://stackoverflow.com/questions/27548153/adjusting-x-axis-label-or-y-axis-label-font-font-size-bokeh)
        self.fig.xaxis.visible = None
        self.fig.yaxis.visible = None
        
        self.fig.add_layout(self._xaxis, 'below')
        self.fig.add_layout(self._yaxis, 'left')
        
        # we need this to have ticks on cbar at init
        # bokeh has no mecanisme to have a callback at init time, so self.__codec can't be called.
        # see https://github.com/bokeh/bokeh/issues/4272
        if self._color_mapper:
            if self._color_mapper.low is None:
                self._color_mapper.low=np.min(self._view.source.data[self.c])
            if self._color_mapper.high is None:
                self._color_mapper.high=np.max(self._view.source.data[self.c])
        
        if self._colorbar:
            self.fig.add_layout(self._colorbar, 'right')
        
        
    
        
        self._transform_x=self.x
        self._transform_y=field(self.y,self._scatter_type_transform_y)
        if self.c:
            self._transform_c=field(self.c,self._color_mapper)
        if self.s:
            self._transform_s=field(self.s,self._transform_s_js)
        else:
            self._transform_s=10
        
        self.scatter=self.fig.scatter(x=self._transform_x ,y= self._transform_y,  size=self._transform_s ,line_color=None, fill_color=self._transform_c,nonselection_fill_color=self._transform_c,nonselection_fill_alpha=0.03,source=self._source,view=self._view,**self.__kwargs)
       
       
        # callbacks allowing axis change
        self._xaxis_select.js_on_change('value', CustomJS(args=dict(scatter=self.scatter, axis=self._xaxis, view=self._view), code=self.__codex))
        self._yaxis_select.js_on_change('value', CustomJS(args=dict(scatter=self.scatter, axis=self._yaxis, view=self._view), code=self.__codey))
        if self.c:
            self._caxis_select.js_on_change('value', CustomJS(args=dict(scatter=self.scatter, view=self._view , color_mapper=self._color_mapper, fig=self.fig), code=self.__codec))
        if self.s:
            self._size_select.js_on_change('value', CustomJS(args=dict(scatter=self.scatter, size_slider=self._size_slider, view=self._view), code=self.__codes))
            self._size_slider.js_on_change('value', CustomJS(args=dict(scatter=self.scatter), code="scatter.glyph.change.emit();"))
    
        # callback on checkbox match_aspect
        self.fig.match_aspect=self._fig_match_aspect
        self._fig_match_aspect_chekbox.js_on_change('active',CustomJS(args=dict(fig=self.fig,scatter=self.scatter), code=self.__code_fig_match_aspect))
        
        # Layout widgets next to the plot
        controls_list=[]
        if len(self.x_cols) > 1:
            controls_list.append(self._xaxis_select)
        if len(self.y_cols) > 1:
            controls_list.append(self._yaxis_select)
        if len(self.c_cols) > 1:
            controls_list.append(self._caxis_select)
        if len(self.s_cols) > 1:
            controls_list.append(self._size_select)
            controls_list.append(self._size_slider)
        if len(self._scatter_types) > 1:
            controls_list.append(self._scatter_type_select)
        if self._fig_match_aspect is None:
            controls_list.append(self._fig_match_aspect_chekbox)
        controls = column(controls_list)
        
        self.widget=layout([ controls, self.fig])
        
