#!/usr/bin/env python
from __future__ import print_function
from bokeh.sampledata.iris import flowers
from bokeh.models import ColumnDataSource, CDSView, LassoSelectTool
from bokeh.plotting import output_file,save
from bokeh.layouts import layout,column
import bkwidgets


if __name__ == "__main__":
    source=ColumnDataSource(flowers)
    source.remove('index')
    output_file("flowers.html")

    # build filter selectors from source
    selectors=bkwidgets.Filter(source)
    # setup a view connected to selectors
    view=CDSView(source=source,filters=selectors.filters)
    # two scatterplot using the same view, with lasso tool 
    scatter1=bkwidgets.ScatterAxis(source=source,view=view)
    scatter1.fig.add_tools(LassoSelectTool())
    scatter2=bkwidgets.ScatterAxis(source=source,view=view,x="sepal_length",y="petal_length",c="sepal_width")
    scatter2.fig.add_tools(LassoSelectTool())
    
    # layout, and save
    p=layout([[selectors.widget,scatter1.widget,scatter2.widget]])
    save(p)
    print("flowers.html dumped")